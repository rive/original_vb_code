# RIVE Original Vb Code

Historical version of the RIVE code (Visual Basic language) - This version is no longer kept up to date with the unified pyRIVE and C-RIVE versions of the code.
See RIVE website https://www.federation-fire.cnrs.fr/rive/

# Author

Gilles.Billen@sorbonne-universite.fr (CNRS - UMR7619 METIS)


# License
 
This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0, or the GNU General Public License,
version 3 or any later version with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
SPDX-License-Identifier: EPL-2.0 OR GPL-3.0-or-later WITH Classpath-exception-2.0
