'**************************************************************************
'* ------------------------------------------------------------------------
'*  Author ......: Gilles.Billen@sorbonne-universite.fr (UMR 7619 - CNRS)
'*  Program .....: modRIVE.mod (visual basic)
'*  Description .....: 
'*  - benthic processes
'*  - phytoplankton growth
'*  - biogeo processes in water column
'* -----------------------------------------------------------------------
'*  - calcmuf()
'*  - calcRIVE()
'*  - physiotemp()
'*  - parameters()
'*  - MetafluMariThou()
'*  - oxysat()
'*  - ftp()
'* ------------------------------------------------------------------------
'* License:
'* ------------------------------------------------------------------------
'* This program and the accompanying materials are made available under the
'* terms of the Eclipse Public License 2.0 which is available at
'* http://www.eclipse.org/legal/epl-2.0, or the GNU General Public License,
'* version 3 or any later version with the GNU Classpath Exception which is available
'* at https://www.gnu.org/software/classpath/license.html.
'* SPDX-License-Identifier: EPL-2.0 OR GPL-3.0-or-later WITH Classpath-exception-2.0

'*************************************************************************


Private Sub calcmuf(DIA, GRA, CYA, MES, NO3, NH4, PO4, SIO, prof, d)

    Dim flimnutdia!, flimnutgra!, flimnutcya!
    Dim ssidia!, ssigra!, ssicya!
    Dim sridia!, srigra!, sricya!
   
     If mich((NH4 + NO3), Kpndia) > mich(PO4, Kppdia) Then
         flimnutdia = mich(PO4, Kppdia)
     Else
         flimnutdia = mich((NH4 + NO3), Kpndia)
     End If
     If mich(SIO, Kpsi) < flimnutdia Then flimnutdia = mich(SIO, Kpsi)

     If mich((NH4 + NO3), Kpngra) > mich(PO4, Kppgra) Then
         flimnutgra = mich(PO4, Kppgra)
     Else
         flimnutgra = mich((NH4 + NO3), Kpngra)
     End If

     If mich((NH4 + NO3), Kpncya) > mich(PO4, Kppcya) Then
         flimnutcya = mich(PO4, Kppcya)
     Else
         flimnutcya = mich((NH4 + NO3), Kpncya)
     End If

     PHY = DIA + GRA + CYA
     eta = fcteta(MES, PHY)
       
        lamda = L1 * (1 - L2 * Cos(W * d))
        J0 = J1 * (1 - J2 * Cos(W * d))

        cmufdia = 0
        cmufgra = 0
        cmufcya = 0
        excrphy = 0

        stp = 0.5
        Dim Hour
        For Hour = 1 To 24 Step stp
             I0 = 0: ssidia = 0: ssigra = 0: ssicya = 0
    
             If Hour < lamda Then
                     I0 = pi / 2 * J0 * Sin(pi * Hour / lamda)
                     For z = 0 To prof Step 0.02
                        IL = I0 * F_exp(-eta * z)
                        photdia = kmaxdia * (1 - F_exp(-alfdia * IL / kmaxdia))
                        ssidia = ssidia + photdia / 50
                        photgra = kmaxgra * (1 - F_exp(-alfgra * IL / kmaxgra))
                        ssigra = ssigra + photgra / 50
                        photcya = kmaxcya * (1 - F_exp(-alfcya * IL / kmaxcya))
                        ssicya = ssicya + photcya / 50
                    
                     Next z
                     ssidia = ssidia / prof: 'h-1
                     ssigra = ssigra / prof
                     ssicya = ssicya / prof
             End If
            
             sridia = srmaxdia * mich(Sd, ksdia)
             srigra = srmaxgra * mich(Sg, ksgra)
             sricya = srmaxcya * mich(Sc, kscya)
             cridia = kcrdia * Rd
             crigra = kcrgra * Rg
             cricya = kcrcya * Rc
             muhdia = mufmaxdia * mich(Sd, ksdia) * flimnutdia
             muhgra = mufmaxgra * mich(Sg, ksgra) * flimnutgra
             muhcya = mufmaxcya * mich(Sc, kscya) * flimnutcya
             respdia = maintdia + cespdia * muhdia
             respgra = maintgra + cespgra * muhgra
             respcya = maintcya + cespcya * muhcya
             excrdia = 0.02 / 35 * ssidia + 0.038 / 35
             excrgra = 0.02 / 35 * ssigra + 0.038 / 35
             excrcya = 0.02 / 35 * ssicya + 0.038 / 35
    
             If Sd = 0 Then respdia = maintdia: muhdia = -(respdia + excrdia)
             If Sg = 0 Then respgra = maintgra: muhgra = -(respgra + excrgra)
             If Sc = 0 Then respcya = maintcya: muhcya = -(respcya + excrcya)
            
             Sd = Sd + (ssidia - respdia - sridia + cridia - muhdia - excrdia) * stp
             If Sd > 3 Then excrdia = excrdia + (Sd - 3): Sd = 3
             If Sd < 0 Then muhdia = muhdia + Sd / stp: Sd = 0
             Rd = Rd + (sridia - cridia) * stp
             If Rd < 0 Then Rd = 0
             cmufdia = cmufdia + muhdia * stp
             
             Sg = Sg + (ssigra - respgra - srigra + crigra - muhgra - excrgra) * stp
             If Sg > 3 Then excrgra = excrgra + (Sg - 3): Sg = 3
             If Sg < 0 Then muhgra = muhgra + Sg / stp: Sg = 0
             Rg = Rg + (srigra - crigra) * stp
             If Rg < 0 Then Rg = 0
             cmufgra = cmufgra + muhgra * stp
            
             Sc = Sc + (ssicya - respcya - sricya + cricya - muhcya - excrcya) * stp
             If Sc > 3 Then excrcya = excrcya + (Sc - 3): Sc = 3
             If Sc < 0 Then muhcya = muhcya + Sc / stp: Sc = 0
             Rc = Rc + (sricya - cricya) * stp
             If Rc < 0 Then Rc = 0
             cmufcya = cmufcya + muhcya * stp
    
             excrphy = excrphy + (excrdia * DIA + excrgra * GRA + excrcya * CYA) * stp

        Next Hour

        cmufdia = cmufdia / 24
        cmufgra = cmufgra / 24
        cmufcya = cmufcya / 24
        excrphy = excrphy / 24
        
End Sub

Private Sub calcRIVE(dilu, prof, dlxt, DIA, GRA, CYA, MES, HD1, HD2, HD3, HP1, HP2, HP3, DSS, BAG, BAP, NIT, NAT, FEL, FEA, BFE, ZOR, ZOC, OXY, NO3, NH4, PIT, PO4, SIO, SIB, SED, HB1, HB2, HB3, BBS, BPI, NO2, N2O, CH4)

  
  If DIA = 0 Then DIA = 0.001
  If GRA = 0 Then GRA = 0.001
  If CYA = 0 Then CYA = 0.0001
  If BAP = 0 Then BAP = 0.000001
  If NIT = 0 Then NIT = 0.000001
  PHY = DIA + GRA + CYA
  
  If DIA > 200 / cvcchl Then cmufdia = 0 'mod GB 23/01/07
  If GRA > 200 / cvcchl Then cmufgra = 0
  If CYA > 200 / cvcchl Then cmufcya = 0
  
  If DIA < 5 / cvcchl Then virusflagdia = 0 'mod GB 23/01/07 extinction des virus si PHY redescendu
  If GRA < 5 / cvcchl Then virusflaggra = 0
  If CYA < 5 / cvcchl Then virusflagcya = 0
  

        'calculation of increments
    
'suspended matter & benthic pools  mod GB aot 2006

sedimMES = kMESsed * MES: 'mg/l/h
If sedimMES > MES / st Then sedimMES = MES / st
If MES < CapMES And SED > SEDo Then
     erosMES = kMESsed * (CapMES - MES) * (SED - SEDo) / (SEDo)
Else
     erosMES = 0
End If
If erosMES > 0.5 * SED / prof / st Then erosMES = 0.5 * SED / prof / st: 'g/m/m/h= mg/l/h OK
If SED > SEDo Then
  txeros = erosMES * prof / SED: 'taux d'rosion du stock benthique,  h-1
Else
  txeros = 0
End If

DMES = -sedimMES + erosMES: 'mg/l/h = g/m3/h

tass = tassmax * (SED - SEDo) / SEDo

If SED < SEDo Then tass = 0

DSED = (sedimMES - erosMES) * prof - tass * SED: ' g/m/h

sedimBAP = sedBAP * BAP: 'mgC/l/h
sedimBAG = sedBAG * BAG
sedimNIT = sedNIT * NIT
sedimNAT = sedNAT * NAT
sedimDIA = sedDIA * DIA
sedimGRA = sedGRA * GRA
sedimCYA = sedCYA * CYA
sedimHP1 = kHPsed * HP1
sedimHP2 = kHPsed * HP2
sedimHP3 = kHPsed * HP3
sedimFEA = sedBAG * FEA: 'nb/l/h=1000b/m2/h pour 1 m de prof  GB 2/2/10

sedimPIT = sedimMES * (PIT - PO4) / MES: 'mol/l/h

DHB1 = (epsd1 + epsp1) * (sedimBAP + sedimBAG + sedimNIT + sedimNAT + sedimDIA + sedimGRA + sedimCYA) * prof + sedimHP1 * prof - txeros * HB1 - tass * HB1 - k1b * HB1: 'gC/m/h corr GB 23 fev 2009 suite remarque Alice VanHoute
DHB2 = (epsd2 + epsp2) * (sedimBAP + sedimBAG + sedimNIT + sedimNAT + sedimDIA + sedimGRA + sedimCYA) * prof + sedimHP2 * prof - txeros * HB2 - tass * HB2 - k2b * HB2: 'gC/m/h
DHB3 = (epsd3 + epsp3) * (sedimBAP + sedimBAG + sedimNIT + sedimNAT + sedimDIA + sedimGRA + sedimCYA) * prof + sedimHP3 * prof - txeros * HB3 - tass * HB3: 'gC/m/h
DBBS = sedimDIA * prof / 12 * 1000 * mSiC + sedimMES * SIB / MES * prof - txeros * BBS - tass * BBS - kbSi * (1 - SIO / SiOsat) * BBS: 'mmolSi/m/h
DBPI = sedimPIT * prof - txeros * BPI - tass * BPI + (k1b * HB1 + k2b * HB2) / 40 / 31 * 1000 + FlPO4bent - FlPO4bentzf: 'mmolP/m/h  rajouter la production par degradation de MO, enlever le flux bentique vers la colonne d'eau et ajouter le flux benthique depuis la couche compacte
DBFE = sedimFEA * prof - txeros * BFE - tass * BFE - 0.5 * kdfec * BFE: '1000bf/m2/h GB 2/2/10

'zooplankton
crzor = muzoptr * (PHY - phy0r) / (PHY - phy0r + Kphyr) * ZOR
crzoc = muzoptc * (PHY - phy0c) / (PHY - phy0c + Kphyc) * ZOC
If PHY < phy0r Then crzor = 0
If PHY < phy0c Then crzoc = 0
grazr = grmaxr * (PHY - phy0r) / (PHY - phy0r + Kphyr) * ZOR
grazc = grmaxc * (PHY - phy0c) / (PHY - phy0c + Kphyc) * ZOC
If PHY < phy0r Then grazr = 0
If PHY < phy0c Then grazc = 0
If OXY < 30 Then crzor = 0: grazr = 0: crzoc = 0: grazc = 0
respZOO = grazr - crzor + grazc - crzoc
DZOR = crzor - kdzr * ZOR - grbenth * moul / prof * ZOR
DZOC = crzoc - kdzc * ZOC

'phytoplankton 
If (virusflagdia = 1 Or DIA > 200 / cvcchl) And d > 15 Then
   kdfcdia = 10 * kdfdia: virusflagdia = 1
Else
   kdfcdia = kdfdia
End If

If (virusflaggra = 1 Or GRA > 200 / cvcchl) Then
   kdfcgra = 10 * kdfgra: virusflaggra = 1
Else
   kdfcgra = kdfgra
End If

If (virusflagcya = 1 Or CYA > 60 / cvcchl) Then
   kdfccya = 10 * kdfcya: virusflagcya = 1
Else
   kdfccya = kdfcya
End If

grmouldia! = (grbenth * moul / prof) * (DIA - (phy0r / 3))
grmoulgra! = (grbenth * moul / prof) * (GRA - (phy0r / 3))
grmoulcya! = (grbenth * moul / prof) * (CYA - (phy0r / 3))

If DIA < (phy0r / 3) Then grmouldia = 0
If GRA < (phy0r / 3) Then grmoulgra = 0
If CYA < (phy0r / 3) Then grmoulcya = 0

'Gestion de la mortalit (grazing : broutage)
DDIA = (cmufdia - kdfcdia - sedDIA) * DIA - grmouldia - (grazc + grazr) * DIA / PHY
DGRA = (cmufgra - kdfcgra - sedGRA) * GRA - grmoulgra - (grazc + grazr) * GRA / PHY
DCYA = (cmufcya - kdfccya - sedCYA) * CYA - grmoulcya - (grazc + grazr) * CYA / PHY


'silice  cor GB aout 2006 puis nov 06 unit SIB vs DIA
DSIO = -cmufdia * DIA / 12 * mSiC * 1000 + kbSi * (1 - SIO / SiOsat) * SIB - FlSiObent / prof: 'inversion du flx benthique!!! corr GB/MT 13/03/08
DSIB = (kdfcdia * DIA + (grazr + grazc) * DIA / PHY + (grbenth * moul / prof) * DIA) / 12 * mSiC * 1000 - sedimMES * SIB / MES + txeros * BBS / prof - kbSi * (1 - SIO / SiOsat) * SIB

'org.matter and bacteria  mod GB aot 06 pour HP123
If OXY >= 30 Then rdmts = Ybs: rdmtl = Ybl
If OXY < 30 And NO3 >= 15 Then rdmts = Ybs / 2: rdmtl = Ybl / 2
If OXY < 30 And NO3 < 15 Then rdmts = Ybs / 3: rdmtl = Ybl / 3
kdbbs = kdbs * rdmts / Ybs: kdbbl = kdbl * rdmtl / Ybl

BAT = BAP + BAG: 'mod GB sept 08 il y avait cBAT!!! ce qui annulait l'hydrolyse!
hydr1d = e1maxs * mich(HD1, KH1s) * BAT
hydr2d = e2maxs * mich(HD2, KH2s) * BAT

DHD1 = epsd1 * (kdfcdia * DIA + kdfcgra * GRA + kdfccya * CYA + kdbbs * BAP + kdbbl * BAG + kdnit * NIT + kdzr * ZOR + kdzc * ZOC) - hydr1d + k1b * HP1
DHD2 = epsd2 * (kdfcdia * DIA + kdfcgra * GRA + kdfccya * CYA + kdbbs * BAP + kdbbl * BAG + kdnit * NIT + kdzr * ZOR + kdzc * ZOC) - hydr2d + k2b * HP2
DHD3 = epsd3 * (kdfcdia * DIA + kdfcgra * GRA + kdfccya * CYA + kdbbs * BAP + kdbbl * BAG + kdnit * NIT + kdzr * ZOR + kdzc * ZOC)

DHP1 = epsp1 * (kdfcdia * DIA + kdfcgra * GRA + kdfccya * CYA + kdbbs * BAP + kdbbl * BAG + kdnit * NIT + kdzr * ZOR + kdzc * ZOC) - k1b * HP1 - sedimHP1 + txeros * HB1 / prof
DHP2 = epsp2 * (kdfcdia * DIA + kdfcgra * GRA + kdfccya * CYA + kdbbs * BAP + kdbbl * BAG + kdnit * NIT + kdzr * ZOR + kdzc * ZOC) - k2b * HP2 - sedimHP2 + txeros * HB2 / prof
DHP3 = epsp3 * (kdfcdia * DIA + kdfcgra * GRA + kdfccya * CYA + kdbbs * BAP + kdbbl * BAG + kdnit * NIT + kdzr * ZOR + kdzc * ZOC) - sedimHP3 + txeros * HB3 / prof

uptbs = bmaxs * mich(DSS, Kss) * BAP
uptbl = bmaxl * mich(DSS, Ksl) * BAG
DDSS = hydr1d + hydr2d - uptbs - uptbl + excrphy: 'mod GB nov 06 excrphy est en mgC/l/h

respHet = (1 - rdmts) * uptbs + (1 - rdmtl) * uptbl: 'mgC/l.h
DBAP = rdmts * uptbs - kdbbs * BAP - sedimBAP
DBAG = rdmtl * uptbl - kdbbl * BAG - sedimBAG

'nitrif. bacteria
Nitros = munitmax / Ynit * mich(NH4, kNH4nit) * mich(OXY, kO2nit) * NIT
If Nitros > NH4 / st Then Nitros = NH4 / st
If Nitros > 3 / 2 * OXY / st Then Nitros = 3 * OXY / st
Nitrat = munatmax / Ynat * mich(NO2, kNO2nat) * mich(OXY, kO2nat) * NAT
If Nitrat > NO2 / st Then Nitrat = NO2 / st
If Nitrat > OXY / 2 / st Then Nitrat = OXY / st: 'mod GB 18 dec 2010
If OXY < kO2nat Then
  Nitrosbis! = munitmax / Ynit / 10 * mich(NH4, kNH4nit) * mich(NO2, kNO2nat) * mich(OXY, kO2nit) * NIT: 'modif GB 18 dec 2010
End If

'oxygen
Rea = krea * (oxysat(Temp) - OXY)
Actorg = (respHet + respZOO) / 12 * 1000: 'micromolO2/l.h
phot = (cmufdia * DIA + cmufgra * GRA + cmufcya * CYA) * 1.25 / 12 * 1000: 'micromolO2/l.h
DOXY = Rea + phot - Actorg - Nitros * 3 / 2 - Nitrat * 1 / 2 - FlO2bent / prof - grbenth * moul / prof * (PHY + ZOR) * 0.8 / 12 * 1000: 'micromolO2/l.h
If DOXY < 0 And -(DOXY) > (OXY - 60) / st Then
        Nitros = 0: Nitrat = 0
        denit! = (Actorg - (OXY - 60 + Rea + phot)) * 4 / 5
        If denit < 0 Then denit = 0
        DOXY = 0
Else: denit = 0
End If

DNIT = Ynit * Nitros - kdnit * NIT - sedimNIT
DNAT = Ynat * Nitrat - kdnat * NAT - sedimNAT

'N2O GB 2/2/10
pNden = 0.025 '(molN2O/molNO3)
prodN2Oden = pNden * denit
ventN2O = kN2O * (N2O - N2Oeq) / prof: 'm/h*mol/l /m=mol/l/h

DN2O = -ventN2O + prodN2Oden + Nitrosbis + intdenit * pNden / prof

'Methane modif ft 0908
ventCH4 = kCH4 * (CH4 - CH4eq) / prof
FlNH4seuil = 10 * (tass * SED * 0.2 / 4 + Df * 400 / zf) 'mol/L
kCH4bent = 4 'molCH4/molNH4
If (-FlNH4bent - FlNH4seuil) > 0 Then prodbentCH4 = kCH4bent * (-FlNH4bent - FlNH4seuil) Else prodbentCH4 = 0

DCH4 = -ventCH4 + prodbentCH4

'Nitrate, nitrite, Ammonium, oxyde nitreux
uptphyN = (cmufdia * DIA + cmufgra * GRA + cmufcya * CYA + excrphy) / 7 / 14 * 1000: 'micromolN/l.h
If NH4 > 0 And NO3 > 0 Then
  uptphyNH4 = uptphyN * (NH4 / (NH4 + NO3)) ^ (0.025)
Else
  uptphyNH4 = 0
End If
uptphyNO3 = uptphyN - uptphyNH4
uptbactN = (rdmts * uptbs + rdmtl * uptbl) / 7 / 14 * 1000
Ammonif = ((1 - rdmts) * uptbs + (1 - rdmtl) * uptbl + respZOO) / 7 / 14 * 1000 + grbenth * moul / prof * (PHY + ZOR) * 0.5 / 7 / 14 * 1000: 'micromolN/l.h
DNO3 = -denit - FlNO3bent / prof - uptphyNO3 + Nitrat
DNH4 = Ammonif - uptbactN - FlNH4bent / prof - uptphyNH4 - Nitros
DNO2 = Nitros - Nitrat

'Phosphorus
uptphyPO4 = (cmufdia * DIA + cmufgra * GRA + cmufcya * CYA + excrphy) / 40 / 31 * 1000
uptbactP = (rdmts * uptbs + rdmtl * uptbl) / 40 / 31 * 1000
Prelease = ((1 - rdmts) * uptbs + (1 - rdmtl) * uptbl + respZOO) / 40 / 31 * 1000 + grbenth * moul / prof * (PHY + ZOR) * 0.5 / 40 / 31 * 1000: 'micromolP/l.h
DPIT = Prelease - FlPO4bent / prof - uptphyPO4 - uptbactP - sedimPIT + txeros * BPI / prof

'bact fcales
DFEL = -kdfec * FEL
DFEA = -0.5 * kdfec * FEA - sedimFEA + txeros * BFE / prof: 'nb/l/h

'INCREMENTATION

   MES = MES + st * (DMES - dilu * (MES - sstarMES))
     If MES < 0.1 Then MES = 0.1
   DIA = DIA + st * (DDIA - dilu * (DIA - sstarDIA))
     If DIA < 0.0001 Then DIA = 0.0001
   GRA = GRA + st * (DGRA - dilu * (GRA - sstarGRA))
     If GRA < 0.0001 Then GRA = 0.0001
   CYA = CYA + st * (DCYA - dilu * (CYA - sstarCYA))
     If CYA < 0.0001 Then CYA = 0.0001
   BAP = BAP + st * (DBAP - dilu * (BAP - sstarBAP))
     If BAP < 0.0001 Then BAP = 0.0001
     
   BAG = BAG + st * (DBAG - dilu * (BAG - sstarBAG))
     If BAG < 0 Then BAG = 0
   HD1 = HD1 + st * (DHD1 - dilu * (HD1 - sstarHD1))
     If HD1 < 0 Then HD1 = 0  ' ajout GB 13 dec 2010
   HD2 = HD2 + st * (DHD2 - dilu * (HD2 - sstarHD2))
     If HD2 < 0 Then HD2 = 0
   HD3 = HD3 + st * (DHD3 - dilu * (HD3 - sstarHD3))
   HP1 = HP1 + st * (DHP1 - dilu * (HP1 - sstarHP1))
     If HP1 < 0 Then HP1 = 0
   HP2 = HP2 + st * (DHP2 - dilu * (HP2 - sstarHP2))
     If HP2 < 0 Then HP2 = 0
   HP3 = HP3 + st * (DHP3 - dilu * (HP3 - sstarHP3))
     If HP3 < 0 Then HP3 = 0
   DSS = DSS + st * (DDSS - dilu * (DSS - sstarDSS))
     If DSS < 0 Then DSS = 0
   OXY = OXY + st * (DOXY - dilu * (OXY - sstarOXY))
     If OXY < 0 Then OXY = 0
   NH4 = NH4 + st * (DNH4 - dilu * (NH4 - sstarNH4))
     If NH4 < 0 Then NH4 = 0
   NO3 = NO3 + st * (DNO3 - dilu * (NO3 - sstarNO3))
     If NO3 < 0 Then NO3 = 0
   PIT = PIT + st * (DPIT - dilu * (PIT - sstarPIT))
     If PIT < 0 Then PIT = 0
     PO4 = phosph(PIT, MES)
   SIO = SIO + st * (DSIO - dilu * (SIO - sstarSIO))
     If SIO < 0 Then SIO = 0
     If SIO > SiOsat Then SIO = SiOsat
   SIB = SIB + st * (DSIB - dilu * (SIB - sstarSIB))
     If SIB < 0 Then SIB = 0
   ZOR = ZOR + st * (DZOR - dilu * (ZOR - sstarZOR))
     If ZOR < 0.000001 Then ZOR = 0.000001
   ZOC = ZOC + st * (DZOC - dilu * (ZOC - sstarZOC))
     If ZOC < 0.000001 Then ZOC = 0.000001
   NIT = NIT + st * (DNIT - dilu * (NIT - sstarNIT))
     If NIT < 0.0000001 Then NIT = 0.0000001
   NAT = NAT + st * (DNAT - dilu * (NAT - sstarNAT))
     If NAT < 0.0000001 Then NAT = 0.0000001
   NO2 = NO2 + st * (DNO2 - dilu * (NO2 - sstarNO2))
     If NO2 < 0.0000001 Then NO2 = 0.0000001
   N2O = N2O + st * (DN2O - dilu * (N2O - sstarN2O))
     If N2O < 0.00000001 Then N2O = 0.00000001
   CH4 = CH4 + st * (DCH4 - dilu * (CH4 - sstarCH4))
     If CH4 < 0.00000001 Then CH4 = 0.00000001
   
   FEL = FEL + st * (DFEL - dilu * (FEL - sstarFEL))
     If FEL < 0 Then FEL = 0
   FEA = FEA + st * (DFEA - dilu * (FEA - sstarFEA))
     If FEA < 0 Then FEA = 0
     
   SED = SED + st * 240 / dlxt * DSED
     If SED <= 0 Then SED = SEDo
   HB1 = HB1 + st * 240 / dlxt * DHB1
     If HB1 <= 0 Then HB1 = ctnHB1 * SED / 1000
   HB2 = HB2 + st * 240 / dlxt * DHB2
     If HB2 <= 0 Then HB2 = ctnHB2 * SED / 1000
   HB3 = HB3 + st * 240 / dlxt * DHB3
     If HB3 <= 0 Then HB3 = ctnHB3 * SED / 1000
   BBS = BBS + st * 240 / dlxt * DBBS
     If BBS <= 0 Then BBS = ctnBBS * SED / 1000
   BPI = BPI + st * 240 / dlxt * DBPI
     If BPI <= 0 Then BPI = ctnBPI * SED / 1000
   BFE = BFE + st * 240 / dlxt * DBFE
     If BFE <= 0 Then BFE = ctnBFE * SED / 1000: 'GB 2/2/10
   End Sub

Private Sub physiotemp(Temp)

ftpdia! = ftp(Temp, toptdia, dtidia)
kmaxdia = ftpdia! * kmax20dia
srmaxdia = ftpdia! * srmax20dia
kcrdia = ftpdia! * kcr20dia
mufmaxdia = ftpdia! * mufmax20dia
maintdia = ftpdia! * maint20dia
kdfdia = ftpdia! * kdf20dia

ftpgra! = ftp(Temp, toptgra, dtigra)
kmaxgra = ftpgra! * kmax20gra
srmaxgra = ftpgra! * srmax20gra
kcrgra = ftpgra! * kcr20gra
mufmaxgra = ftpgra! * mufmax20gra
maintgra = ftpgra! * maint20gra
kdfgra = ftpgra! * kdf20gra

ftpcya! = ftp(Temp, toptcya, dticya)
kmaxcya = ftpcya! * kmax20cya
srmaxcya = ftpcya! * srmax20cya
kcrcya = ftpcya! * kcr20cya
mufmaxcya = ftpcya! * mufmax20cya
maintcya = ftpcya! * maint20cya
kdfcya = ftpcya! * kdf20cya

ftpzor! = ftp(Temp, toptzr, dtizr)
muzoptr = ftpzor! * muzopt20r
grmaxr = ftpzor! * grmax20r
kdzr = ftpzor! * kdz20r

ftpzoc! = ftp(Temp, toptzc, dtizc)
muzoptc = ftpzoc! * muzopt20c
grmaxc = ftpzoc! * grmax20c
kdzc = ftpzoc! * kdz20c

ftpbenth! = ftp(Temp, toptbenth, dtibenth)
grbenth = ftpbenth * grbenth20

ftpbacs! = ftp(Temp, toptbs, dtibs)
e1maxs = ftpbacs! * e1max20s
e2maxs = ftpbacs! * e2max20s
bmaxs = ftpbacs! * bmax20s
kdbs = ftpbacs! * kdb20s
k1b = ftpbacs! * k1b20
k1p = ftpbacs! * k1p20

ftpbacl! = ftp(Temp, toptbl, dtibl)
e1maxl! = ftpbacl! * e1max20l
e2maxl! = ftpbacl! * e2max20l
bmaxl = ftpbacl! * bmax20l
kdbl = ftpbacl! * kdb20l
ftpfec = ftp(Temp, toptfec, dtifec)
kdfec = ftpfec * kdfec20

ftpnit! = ftp(Temp, toptnit, dtinit)
munitmax = ftpnit! * munitmax20
munatmax = ftpnit! * munatmax20
kdnit = ftpnit! * kd20nit
kdnat = ftpnit! * kd20nat
kni = ftpnit! * kNi20

ftpsi! = F_exp(60000 / 8.314 * (1 / 275 - 1 / (273 + Temp))):  ' selon Rickert et al. 2002  cor GB aout 2006
kbSi = ftpsi! * kbSi20

End Sub

        'Lecture fichier lumiere (.LUM) : : paramètres lum
        lumiere$ = g_path + "\datas\params\soleil.lum"
        Open lumiere For Input As #10
        Line Input #10, zz$
        Input #10, lummoy$, ampli$, photopmoy$, ampli$  '1 ligne de titres
        Input #10, unitlum$, ampli$, photopmoy$, ampli$ '1 ligne d'unités
        Input #10, J1, J2, L1, L2
        Close #10
        
        'Option lecture du fichier lumière de l'année de simulation; Nejla aout 2010
        'fichierdebit$ = ReadScn("scn" + Scenario, "ECOU") ' renvoie le nom du fichier écoulement
        'anneelumiere$ = Right$(fichierdebit, 4)
        'lumiere$ = g_path + "\datas\params\" + anneelumiere + ".lum"
        'Open lumiere For Input As #10
        'Line Input #10, zz$
        'Line Input #10, zz$
        'For dec = 1 To 36
        'Input #10, d, Jlum(dec)
        'Next dec
        'Close #10

        'Quelques valeurs de facteurs de conversions
        Alf = 1 / 10 / 24 / 60 / 60 * 1000    'convers (mm/dec*km2) into (m3/s)
        pi = 3.14159265359
        W = 2 * pi / 36
        rnitmes = 0.005 'ratio biomasse nitrif / COTB dans eaux usées
        cvcchl = 1000 / 35  'conv mgC en microgChla
        cvh = 1 / 24 / 3.6              'cv kgC/d.(m3/sec) into mgC/l
        cvn = 1 / 24 / 3.6 / 14 * 1000  'cv kgN/d.(m3/sec) into micromolN/l
        cvp = 1 / 24 / 3.6 / 31 * 1000  'cv kgP/d.(m3/sec) into micromolP/l
        cvf = 1 / 24 / 3.6 * 1000       'cv 10E9bact/d.(m3/sec) into bact/l
        'rspsi = 0.5 / 28 / 24 / 3.6     'mmolSi/hab/sec rejet specifique de silice 0.5 gSi/hab/j "
        cvsi = 1 / 24 / 3.6 / 28 * 1000 'cv kgSi/d.(m3/sec) into micromolSi/l
        csn = 1000 / 7 / 14             'cv mgC into micromolN
        csp = 1000 / 40 / 31            'cv mgC into micromolP

        'Lecture du fichier des parametres physiologiques
        physio$ = g_path + "\datas\params\seneq5.prm" 'SENEQUE 3.7.2!!!! 08/04/2019
        Open physio$ For Input As #10
        Line Input #10, zz$
        Input #10, phytoplankton$, Unit$, DIA$, GRA$, CYA$: 'ligne titre
          Input #10, a$, u$, kmax20dia, kmax20gra, kmax20cya
          Input #10, a$, u$, alfdia, alfgra, alfcya
          Input #10, a$, u$, srmax20dia, srmax20gra, srmax20cya
          Input #10, a$, u$, ksdia, ksgra, kscya
          Input #10, a$, u$, kcr20dia, kcr20gra, kcr20cya
          Input #10, a$, u$, mufmax20dia, mufmax20gra, mufmax20cya
          Input #10, a$, u$, Kppdia, Kppgra, Kppcya
          Input #10, a$, u$, Kpndia, Kpngra, Kpncya
          Input #10, a$, u$, Kpsi, aa, aa
          Input #10, a$, u$, maint20dia, maint20gra, maint20cya
          Input #10, a$, u$, cespdia, cespgra, cespcya
          Input #10, a$, u$, kdf20dia, kdf20gra, kdf20cya
          Input #10, a$, u$, vsdia, vsgra, vscya
          Input #10, a$, u$, toptdia, toptgra, toptcya
          Input #10, a$, u$, dtidia, dtigra, dticya
        Input #10, zooplankton$, Unit$, rotif$, clado$: 'ligne titre
          Input #10, a$, u$, muzopt20r, muzopt20c
          Input #10, a$, u$, Kphyr, Kphyc
          Input #10, a$, u$, phy0r, phy0c
          Input #10, a$, u$, grmax20r, grmax20c
          Input #10, a$, u$, kdz20r, kdz20c
          Input #10, a$, u$, toptzr, toptzc
          Input #10, a$, u$, dtizr, dtizc
        Input #10, grazeurbenthiques$, Unit$, moules$: 'ligne titre
          Input #10, a$, u$, grbenth20
          Input #10, a$, u$, toptbenth
          Input #10, a$, u$, dtibenth
          Input #10, a$, u$, moulMax
        Input #10, bactheterotr$, Unit$, inf1mu$, sup1mu$, FEC$: 'ligne titre
          Input #10, a$, u$, e1max20s, e1max20l
          Input #10, a$, u$, e2max20s, e2max20l
          Input #10, a$, u$, KH1s, KH1l
          Input #10, a$, u$, KH2s, KH2l
          Input #10, a$, u$, bmax20s, bmax20l
          Input #10, a$, u$, Kss, Ksl
          Input #10, a$, u$, Ybs, Ybl
          Input #10, a$, u$, kdb20s, kdb20l, kdfec20
          Input #10, a$, u$, vsbs, vsbl
          Input #10, a$, u$, toptbs, toptbl, toptfec
          Input #10, a$, u$, dtibs, dtibl, dtifec
          Input #10, a$, u$, epsd1, epsd2, epsd3
          Input #10, a$, u$, epsp1, epsp2, epsp3
          Input #10, a$, u$, k1b20
          Input #10, a$, u$, k2b
        Input #10, bactnitrif$, Unit$, NIT$, NAT$: 'ligne titre
          Input #10, a$, u$, munitmax20, munatmax20
          Input #10, a$, u$, kO2nit, kO2nat
          Input #10, a$, u$, kNH4nit, kNO2nat
          Input #10, a$, u$, Ynit, Ynat
          Input #10, a$, u$, kd20nit, kd20nat
          Input #10, a$, u$, vsnit, vsnat
          Input #10, a$, u$, toptnit, toptnat
          Input #10, a$, u$, dtinit, dtinat
        Input #10, adsoptP$, Unit$, Valp$:  'ligne titre
          Input #10, a$, u$, Pac
          Input #10, a$, u$, Kpads
        Input #10, reminSi$, Unit$, Valp$:  'ligne titre
          Input #10, a$, u$, kbSi20
          Input #10, a$, u$, mSiC
          Input #10, a$, u$, toptkSi
          Input #10, a$, u$, dtikSi
        Input #10, reminbenth$, Unit$, Valp$:  'ligne titre
          Input #10, a$, u$, Df
          Input #10, a$, u$, Dc
          Input #10, a$, u$, ds
          Input #10, a$, u$, zf
          Input #10, a$, u$, k1p20
          Input #10, a$, u$, k2p
          Input #10, a$, u$, kNi20
          Input #10, a$, u$, Kam
          Input #10, a$, u$, Kpa
          Input #10, a$, u$, Kpe
          Input #10, a$, u$, alpha
          Input #10, a$, u$, eps
        Input #10, physique$, Unit$, Valp$:  'ligne titre
          Input #10, a$, u$, vso
          Input #10, a$, u$, vsmes
          Input #10, a$, u$, Veli0
          Input #10, a$, u$, Veli1
          Input #10, a$, u$, mDO2
          Input #10, a$, u$, man
        Input #10, retention_riparienne$, Unit$, Valp$:  'ligne titre
          Input #10, a$, u$, toptretrip
          Input #10, a$, u$, dtiretrip
          Input #10, a$, u$, denpot 'SENEQUE 3.7
          Input #10, a$, u$, pNden 'SENEQUE 3.7.2 GB 08/04/2019 ratio N2O/N2 denitrification
          Input #10, a$, u$, kvs 'SENEQUE 3.7.2 coef de ventilation sol-atmosph
        Close #10
        
        'Lecture du fichier des parametres d'initialisation benthiques mod GB août 2006
        benthos$ = g_path + "\datas\params\initbenth37.prm" ' SENEQUE 3.7.1
        Open benthos$ For Input As #11
        Line Input #11, zz$
          Input #11, a$, u$, tassmax
          Input #11, a$, u$, SEDo
          Input #11, a$, u$, ctnHB1
          Input #11, a$, u$, ctnHB2
          Input #11, a$, u$, ctnHB3
          Input #11, a$, u$, ctnBBS
          Input #11, a$, u$, ctnBPI
          Input #11, a$, u$, ctnBFE
          Input #11, a$, u$, Phif ' SENEQUE 3.7.1
          Input #11, a$, u$, dens ' SENEQUE 3.7.1
        Close #11
        ' tass = tassmax ' SENEQUE 3.7.1
End Sub


Sub MetafluMariThou()

zf = (cSED) / ((1 - Phif) * dens)    'm
If cSED < SED Then
tass = 0
Else
tass = tassmax * (cSED - SED) / cSED
End If

'Ammonr : depth-integrated ammonification rate (mmol/m/h)
Ammonr = 1000 / 7 / 14 * ((k1b * cHB1 + k2b * cHB2) + tass * (cHB1 + cHB2))

'Coxd: Carbon oxidant demand (equ/m/h)
Coxd = 4 / 12 * ((k1b * cHB1 + k2b * cHB2) + tass * (cHB1 + cHB2))

'Pminr: depth-integrated rate of organic phosphorus mineralisation (mmol/m/h)
Pminr = 1000 / 40 / 31 * ((k1b * cHB1 + k2b * cHB2) + tass * (cHB1 + cHB2))

'Sidissr: depth-integrated maximum biogenic silica dissolution rate (mmol/m/h)
Sidissr = kbSi * cBBS + tass * cBBS

'Ammonium flux accross the water-sediment interface (mmol/m/h)
fNH4 = (0.9 - 140 * zf ^ 3) 'dimensionless
fnitendo = 15 / 14 * zf * cOXY / oxysat(Temp) * ftp(Temp, toptnit, dtinit) 'mmol/m/h
fnitexo = 1.25 * cNH4 / 1000 * zf / (zf + 0.002) * cOXY / oxysat(Temp) * ftp(Temp, toptnit, dtinit) 'mmol/m/h
FlNH4bent = -fNH4 * Ammonr + fnitendo + fnitexo

'Oxygen flux across the water-sediment interface (mmolO2/m/h)
Nitoxd = 8 / 1000 * (fnitendo + fnitexo)            'equ/m/h
fOXY = 1 - Coxd / (Coxd + 0.00075 * (cOXY / oxysat(Temp)) / zf) 'dimensionless
FlO2bent = 1000 / 4 * (fOXY * Coxd + Nitoxd)

'Nitrate flux across the water-sediment interface (mmol/m/h)
a = 2 * (cNO3 / cOXY) / ((cNO3 / cOXY) + 1.8)       'dimensionless
C = 0.001 * cNO3 * (1 - zf / (zf + 0.0005))             'equ/m/h
fNO3 = a * (1 - Coxd ^ 0.7 / (Coxd ^ 0.7 + C ^ 0.7))      'dimensionless
FlNO3bent = 1000 / 5 * fNO3 * Coxd - fnitendo - fnitexo
            
'Phosphate flux across the water-sediment interface (mmol/m/h)
fPO4 = 1 - (zf ^ 2.5 / (zf ^ 2.5 + 0.032 ^ 2.5))        'dimensionless
FlPO4bent = -fPO4 * Pminr
                    
'Silica flux across the water-sediment interface (mmol/m/h)
fSIO = (1 - cBBS / (cBBS + 1000 / 28 * Exp(0.08 * Temp))) - (0.0003 + 0.00002 * Temp) * cSIO 'dimensionless
FlSiObent = -fSIO * Sidissr

End Sub

Public Function oxysat(Temp)
    oxysat = 475 / (33.5 + Temp) * 1000 / 32: 'micromolO2/l
End Function

Public Function ftp(Temp, topt, dti)
    ftp = F_exp(-((Temp - topt) ^ 2 / dti ^ 2)) / F_exp(-((20 - topt) ^ 2 / dti ^ 2))
End Function
